/*
1.Цикл в програмуванні - це конструкція, яка дозволяє виконувати блок коду повторно, доки задана умова залишається істинною.
2.В JavaScript існують два основних види циклів: цикл for та цикл while , ще є Цикл do-while. for (ініціалізація; умова; інкремент), while (умова), do {} while (умова)
 3. в циклі while умова перевіряється перед входженням у блок коду,  а  в  do-while блок коду виконується принаймні один раз, навіть якщо умова вже в першу ітерацію виявиться як хибна.


//1
let num1, num2;
do {
    num1 = prompt("Введіть перше число");
} while (isNaN(num1));
do {
    num2 = prompt("Введіть друге число");
} while (isNaN(num2));

// Знаходимо менше та більше число
const startNumber = Math.min(num1, num2);
const endNumber = Math.max(num1, num2);

// Вивести на екран всі цілі числа в до б
for (let i = startNumber; i <= endNumber; i++) {
    console.log(i);
}
*/

//2  






