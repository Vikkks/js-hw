//1. Події в JavaScript представляють спосіб реагування на дії користувача або на зміни в браузерному середовищі. 
//Події використовуються для створення інтерактивності в веб-додатках, дозволяючи коду реагувати на різні ситуації.
//2. click, dblclick, mouseover та mouseout, mousedown та mouseup, mousemove
/*element.addEventListener('click', function() {
    console.log('Елемент був натиснутий!');
  });
  element.addEventListener('mouseover', function() {
    console.log('Курсор миші наведено на елемент.');
  });
  element.addEventListener('mouseup', function() {
    console.log('Кнопка миші була відпущена.');
  });*/

//3. Подія "contextmenu" відбувається при спробі  відкрити контекстне меню на елементі (правим натисканням миші). 
//Ця подія надає можливість виконати додаткові дії або заборонити відкриття стандартного контекстного меню браузера.

//1.
const btn = document.getElementById('btn-click')
btn.addEventListener('click', ()=>{
    let p = document.createElement('p')
    p.textContent='New Paragraph'
    let section = document.getElementById('content')
    section.appendChild(p)
})

//2
let btnTwo = document.createElement('button');
btnTwo.id='btn-input-create'
btnTwo.textContent='click'
btnTwo.style.display='block'
btnTwo.style.margin='auto'
let section = document.getElementById('content')
section.appendChild(btnTwo);

btnTwo.addEventListener('click', ()=>{
let input=document.createElement('input');
input.type='password'
input.name='namePass'
input.style.margin = 'auto';
input.style.display = 'block';
btnTwo.parentNode.after(input)
})



