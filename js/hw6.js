//1.Метод об'єкту в програмуванні - це функція, яка є властивістю об'єкта. 
//В об'єкті метод визивається за допомогою запису object.method(), де object - це сам об'єкт, а method - назва методу.
//2.Number, String, Boolean, Array, Object, Function, Null, Undefined. типи даних можуть змінюватися під час виконання програми.
//3.це можна пояснити ситуацією,коли ви присвоюєте об'єкт одній змінній і потім присвоюєте іншій змінній значення цієї першої змінної, 
//обидві змінні тепер вказують на один і той же об'єкт у пам'яті, а не створюються копії цього об'єкта.

//1
/*
const product = {
  name: 'prdName',
  price: 100,
  discount: 20,
  calculateFinalPrice: function () {
    const discountedPrice = this.price - (this.price * this.discount) / 100;
    console.log(`Final Price for ${this.name}: $${discountedPrice}`);
  },
};
product.calculateFinalPrice();

//2.
function greetUser() {
    const name = prompt('Введіть ваше ім:')
    const age = prompt('Введіть ваш вік:')
    const user = {
      name: name,
      age: age,
    };
    const greeting = `Привіт, мені ${user.age} років.`;
    alert(greeting);
  }
  greetUser();

//3
const person = {
    name: 'Stef',
    surname: 'Kazybrid',
    age: 21,
    skills: ['JavaScript', 'React', 'Java'],
    nestedObject: {
        key1: 'value1',
        key2: [1, 2, 3]
    }
}
function deepClone(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }
    const clonedObj = {};
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            clonedObj[key] = deepClone(obj[key]);
        }
    }
    return clonedObj;
}
const person1 = deepClone(person);
person1.name="Ivan"
console.log(person);
console.log(person1);
////3
/*const person = {
    name: 'Stef',
    surname: 'Kazybrid',
    age: 21,
    skills: ['JavaScript', 'React', 'Java'],
    key: {
        key1: 'value1',
        key2: [1, 2, 3],}
}
function processObjects(original, clone){
 clone= structuredClone(original)
 return clone

}
const person1 = processObjects(person,{})
person1.skills.push('TypeScript')
person1.key.key2.pop()
console.log(person)
console.log(person1)*/