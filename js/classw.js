// Обчисліть факторіал числа, яке користувач введе з клавіатури.
/*
let userNumber = 14;
let result = 1;
for (let startNumber = 1; startNumber <= userNumber; ++startNumber) {
  result = result * startNumber;
}
console.log(result);

Написати функцію, яка визначає кількість переданих аргументів, та
повертає їхню суму.

let number1 = 5;
let number2 = 6;

function summ(a, b) {
  return a + b;
}
console.log(summ(number1, number2));

//Завдання 2

const getArgumentsSumm = function () {
  console.log(arguments: ${arguments.length});
  let result = 0;
  for (let i = 0; i < arguments.length; i++) {
    result = result + arguments[i];
  }
  return result;
};
console.log(getArgumentsSumm(34, 67, 89, 12, 56));


Написати функцію-лічильник count.
Функцію має два параметри:
Перший - число, з якого необхідно почати рахунок;
Другий - число, яким необхідно закінчити рахунок.
Якщо число, з якого починається рахунок більше, ніж число,
яким він закінчується, вивести повідомлення:
«⛔️ Помилка! Рахунок неможливий.»
Якщо обидва ці числа однакові, вивести повідомлення:
«⛔️ Помилка! Нема чого рахувати.»
На початку рахунку необхідно вивести повідомлення:
"🏁 Відлік розпочато.".
Кожен "крок" рахунку необхідно виводити в консоль.
Після проходження останнього кроку необхідно вивести повідомлення:
«✅ Відлік завершено».
          
let number1 = 5; 
let number2 = 10; 
 
const counter = function (num1, num2, num3) { 
  if (arguments.length !== 3) { 
    console.log("⛔️ Помилка! Рахунок неможливий."); 
    return; 
  } 
  for (let i = 0; i < arguments.length; i++) { 
    if (typeof arguments[i] !== "number" || isNaN(arguments[i])) { 
      console.log("⛔️ Помилка! Число не допустиме."); 
      return; 
    } 
  } 
  if (num1 > num2) { 
    console.log("⛔️ Помилка! Рахунок неможливий."); 
  } else if (num1 === num2) { 
    console.log("⛔️ Помилка! Нема чого рахувати."); 
  } else { 
    console.log("🏁 Відлік розпочато."); 
 
    for (let i = num1; i <= num2; i++) { 
      if (i % num3 === 0) { 
        console.log(i); 
      } 
    } 
    console.log("✅ Відлік завершено."); 
  } 
}; 
 

let num1 = prompt("num1?")
while(isNaN(num1)){
  num1 = prompt("num1?")
}
let num2 = prompt('num2?')
while(isNaN(num2)){
  num2 = prompt("num1?")
}
function sum(num1,num2){  
  return num1**num2
}
b= sum(num1,num2)
console.log(b)

function pow(x, n) {
  let result = x;

  for (let i = 1; i < n; i++) {
    result *= x;
  }

  return result;
}

let x = prompt("x?", '');
let n = prompt("n?", '');

if (n >= 1 && n % 1 == 0) {
  alert( pow(x, n) );
} else {
  alert(`Степень ${n} не поддерживается, используйте натуральное число`);
}

let ask = (q,y,n) => {
  if (confirm(q)) {y()} else n()
}
ask("Вам есть 18?",
() => alert("Проходите!"),
()=>alert ("Уходите!") );

let user = {age:11}
function isEmpty(obj) {
for(let key in obj){
return true
}
return false
}
console.log(isEmpty(user))

let salaries = {
  John: 100,
  Ann: 160,
  Pete: 130
};

let sum = 0;
for (let key in salaries) {
  sum += salaries[key];
}
alert(sum); // 390

let menu = {
  width: 200,
  height: 300,
  title: "My menu"
};

function multiplyNumeric(obj) {
  for (let key in obj) {
    if (typeof obj[key] == 'number') {
      obj[key] *= 2;
    }
  }
}

console.log('Before:', menu);
multiplyNumeric(menu);
console.log('After:', menu);

function divideUntilTen(num) {
  let iterations = 0;

  while (num >= 10) {
      num /= 2;
      iterations++;
  }

  return iterations;
}
console.log(divideUntilTen(10))

function func(num1, num2) {
	if (num1 > 0 && num2 > 0) {
		return num1 * num2;
	} else {
		return num1 - num2;
	}
}

console.log(func(0, 4));

let obj = {
	x: 1,
	y: 2,
  z: 3
};


let key1 = 'x';
let key2 = 'y';
let key3 = 'z';
let objj ={
  [key1]:1,
  [key2]:2,
  [key3]:3
}
let objectLength = Object.keys(obj).length;
console.log(objectLength)

const user = {
  name: "stef",
  surname : "kazybrid",
  prof:"student",
  sayHi() {
  console.log(`hello my name is ${this.name} ${this.surname}`)
},
changeValueProperty: function (propertyName, propertyValue) {
  if (Object.hasOwn(this, propertyName)) {
    this[propertyName] = propertyValue;
  } else {
    alert("ERROR");
  }
},
addNewProperty(propertyName, propertyValue) {
  if (!Object.hasOwn(this, propertyName)) {
    this[propertyName] = propertyValue;
  } else {
    console.log(`Властивість з таким ім'ям вже існує.`);
  }
},
}
for (let key in user) {
  if (user.hasOwnProperty(key)) {
    console.log(`${key}: ${user[key]}`);
  }
}
user.sayHi();
user.changeValueProperty("name", 'ivan');

console.log(user);

 const isEmpty = (object) => {
  if (typeof object !== 'object' || object === null) {
    // Перевірка, чи переданий аргумент є об'єктом
    return false;
  }

  for (const key in object) {
    // Якщо є хоча б одна властивість, то об'єкт не є порожнім
    if (object.hasOwnProperty(key)) {
      return false;
    }
  }

  return true;
};

// Приклад використання:
console.log(isEmpty({}));           // true
console.log(isEmpty({ a: undefined })); // true
console.log(isEmpty({ a: 1 }));       // false


const person = { 
  age: 29, 
  job: "policeman", 
  clothes: {}, 
}; 
 
let ckeckObject = function (obj) { 
  const values = Object.values(obj); 
  console.log(values); 
  let counterDiff = 0; 
  let counterLight = 0; 
  for (let i = 0; i < values.length; i++) { 
    if ( 
      typeof values[i] !== "string" && 
      typeof values[i] !== "boolean" && 
      typeof values[i] !== "number" && 
      values[i] !== null 
    ) { 
      counterDiff++; 
    } else { 
      counterLight++; 
    } 
  } 
  return { 
    counterDiff, 
    counterLight, 
  }; 
}; 

const days = [ 
  'Monday', 
'Tuesday', 
'Wednesday', 
'Thursday', 
'Friday', 
'Saturday', 
'Sunday', ];
for (let i = 0; i<days.length;i++){
  console.log(days[i])}

for (const value of days) {
  console.log(value); 
}

function week(val){
  console.log(val)
}
days.forEach(week)



const numbers = [1,2,3,4,5,6,7]
const newNumbers = numbers.filter(function(number2){return number2 %2 ==0;})
//const newNumbers = numbers.slice(2)
//const squaredNumbers = numbers.map(function(numbert) { return numbert ** 2;});
console.log(newNumbers)

const words =['s','ss','sss','ss',]
console.log(words.sort())

const days = { 
  ua: [ 'Понеділок', 'Вівторок', 'Середа', 'Четвер','Пятниця', 'Субота', 'Неділя', ], 
  ru: [ 'Понедельник', 'Вторник', 'Среда','Четверг', 'Пятница', 'Суббота', 'Воскресенье', ], 
  en: [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', ], };
  let language = prompt("Please enter language: "); 
 
  function organizer(object, lang) { 
    while (!Object.keys(object).find((value) => value === lang)) { 
      lang = prompt("Please enter correct language: "); 
    } 
    console.log(object); 
  } 
   if(language=='ua')  {
    days.ua.forEach(day => alert(day));  
   }

   

   function createArr(value) {
    const arr = [];
    for (let i = 0; i < value; i++) {
      arr.push(Math.round(Math.random() * 30 + 10));
    }
    return arr;
  }
  console.log(createArr(10));

  function chekArr(arr) {
    console.log(arr);
    let elements = arr.filter((value) => value > 18);
    return elements;
  }
  
  console.log(chekArr(createArr(15)));

  //<input id="button1" type="submit" value="button1">
//</input><input id="button2" type="submit" value="button2">

let btn = document.getElementById('hh')
let elem = document.querySelector('#elem')

console.log(elem.innerHTML)
    
    btn.addEventListener('click', function(){
      elem.type = 'submit';
      console.log(elem.getAttribute('type'))

    })

  // Отримуємо елементи за їхніми ідентифікаторами
const b1 = document.getElementById('1');
const b2 = document.getElementById('2');
const g1 = document.getElementById('3')
const g2 = document.getElementById('4')

g1.addEventListener('click', function(){
  b1.src= b2.src
  console.log(b1)
})
g2.addEventListener('click', function(){
  b2.src= b1.src
  console.log(b2)
})


document.getElementById('calculateBtn').addEventListener('click', function () {
   
  const num2 = parseFloat(document.getElementById('num2').value)
  const num3 = parseFloat(document.getElementById('num3').value) 
  const num4 = parseFloat(document.getElementById('num4').value) 
  const num5 = parseFloat(document.getElementById('num5').value) 
  const average = (num1 + num2 + num3 + num4 + num5) / 5;
});
 // За ідентифікатором (id): елемент з ідентифікатором list;
 let elementById = document.getElementById('list');
 console.log('Елемент за ідентифікатором:', elementById);

 // За класом - елементи з класом list-item;
 let elementsByClass = document.getElementsByClassName('list-item');
 console.log('Елементи за класом:', elementsByClass);

 // По тегу - елементи з тегом li;
 let elementsByTag = document.getElementsByTagName('li');
 console.log('Елементи за тегом:', elementsByTag);

 // За CSS селектором (один елемент) - третій li зі всього списку;
 let elementByCssSelectorSingle = document.querySelector('ul#list li:nth-child(3)');
 console.log('Елемент за CSS селектором (один елемент):', elementByCssSelectorSingle);

 // За CSS селектором (багато елементів) всі доступні елементи li.
 let elementsByCssSelectorMultiple = document.querySelectorAll('ul#list li');
 console.log('Елементи за CSS селектором (багато елементів):', elementsByCssSelectorMultiple);

 // Властивості елемента:
 // innerText;
 console.log('innerText:', elementByCssSelectorSingle.innerText);

 // innerHTML;
 console.log('innerHTML:', elementByCssSelectorSingle.innerHTML);

 // outerHTML.
  document.getElementById('result').textContent = `Average: ${average}`;
  console.log('outerHTML:', elementByCssSelectorSingle.outerHTML);


 let r =document.querySelector('.remove')
 r.classList.remove('remove')
 console.log(r);

 let k =document.getElementsByClassName('bigger')
 k.classList.replace('bigger', 'active');
 console.log(k);

 const text = "item new text"
 const link = '<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google it!</a>'
 
const liatem = [...document.getElementsByClassName('list-item')]
console.log(liatem)


let liatem6 =liatem.find((value)=>value.textContent==='Item 6')

liatem6.textContent=text
let liatem1 = liatem.find((value)=>value.textContent==="Item 7")
liatem1.innerHTML=link
console.log(liatem1)

const store = [...document.getElementsByTagName('li')]

store.forEach((value) => {
  if (value.textContent.endsWith(' 0')) {
    value.innerHTML =
      value.textContent.split(' ')[0] 
    }
})

let s = document.getElementsByTagName('body')[0]
let g =document.createElement('div')
g.textContent = 'Hello, World!'; 
g.classList.add('newclass'); 
s.appendChild(g);
g.style='color:red;font-weight: bold'
setTimeout(() => {
  // Видалення елемента з DOM
  g.remove();
}, 2000); // Видалить елемент через 2 секунди (2000 мілісекунд)

let root = document.getElementById('root')
let parh =document.createElement('div')
parh.textContent='new parahraf'
root.appendChild(parh)

let parh1 = parh.cloneNode(true)
parh1.textContent = 'dddddddddd'
parh.after(parh1)

const squareDiv = document.createElement('div');
squareDiv.className = 'square';
document.body.innerHTML = '';
document.body.prepend(squareDiv);


const kilcsquare = prompt('введіть кількість квадратів')

function getSize () {
    let squareSize;
    while (squareSize === null || squareSize === ''  ||isNaN(squareSize)) {
        squareSize = prompt('Enter square size in px:');
    } return squareSize;
}
size = getSize();
squareDiv.style.cssText = `height: ${size}px; width: ${size}px; border: 1px solid black;`

document.body.innerHTML = "";

function getCountSquare() {
  let countSquare;
  while (
    countSquare === null ||
    countSquare === "" ||
    isNaN(countSquare) ||
    countSquare > 10
  ) {
    countSquare = prompt(
      'Введіть яку кількість квадратів розмістити на екрані:'
    );

    if (countSquare > 10) {
      alert(
      'Кількість квадратів не може бути більше 10. Введіть число заново:'
      );
    }
  }
  return countSquare;
}

let countSquare = getCountSquare();

for (let i = 0; i < countSquare; i++) {
  const squareDiv = document.createElement("div");
  squareDiv.className = "square";
  document.body.prepend(squareDiv);
  let size = getSize();
  let color = getColor();
  squareDiv.style.cssText = `height: ${size}px; width: ${size}px; border: 1px solid black; background-color: rgb(${color});`
}

function getSize() {
  let squareSize;
  while (squareSize === null||  squareSize === ""||  isNaN(squareSize)) {
    squareSize = prompt("Введіть розмір в px:");
  }
  return squareSize;
}

function getColor() {
  let squareColor = ``;
  while (squareColor === null || squareColor === "") {
    squareColor = prompt("Введіть колір квадрату у форматі 255,255,128");
  }
  return squareColor;
}

const LIGHT_CELL = '#ffffff';
const DARK_CELL = '#161619';
const V_CELLS = 8;
const H_CELLS = 8;

function fillChessBoard() {
  const board = document.querySelector('.board');

  if (!board) {
    console.error('Board element not found.');
    return;
  }

  for (let i = 0; i < V_CELLS; i++) {
    for (let j = 0; j < H_CELLS; j++) {
      const cell = document.createElement('div');
      cell.classList.add('cell');

      // Визначення колірності комірки за індексами
      const isDarkCell = (i + j) % 2 === 1;
      cell.style.backgroundColor = isDarkCell ? DARK_CELL : LIGHT_CELL;

      board.appendChild(cell);
    }
  }
}

// Виклик функції для створення шахової дошки
fillChessBoard();

let list = document.createElement("ul"); 
 
for (let i = 0; i < 10; i++) { 
  let listElem = document.createElement("li"); 
  let randomNum = Math.floor(Math.random() * 10) + 10; 
  listElem.textContent = randomNum; 
  list.append(listElem); 
  // console.log(listElem); 
} 
 
let root = document.querySelector("#root"); 
root.append(list);

 const sortedList = list.cloneNode();
 sortedList.className = "sorted";
 let arr = [...list.children];
 arr.sort((a, b) => b.textContent - a.textContent);
 sortedList.append(...arr);
 list.after(sortedList);

 let sum = arr.reduce((acc, value) => acc + +value.textContent, 0) / arr.length
let newElement = document.createElement('li')
newElement.textContent = sum
sortedList.prepend(newElement)

let clonsorted = sortedList.cloneNode()
clonsorted.className ='filtered'
let arr2 = [...sortedList.children]
let arr3 =arr2.filter((element)=>element.textContent>15)
clonsorted.innerHTML=''
clonsorted.append(...arr3)
sortedList.after(clonsorted)


let root = document.getElementById('root')
let btn = document.createElement('button')
btn.textContent='Sign'
btn.style= 'color:red'
root.prepend(btn)
console.log(root)
btn.addEventListener('click', function(){
alert('ласкаво просими')})

btn.addEventListener('mousemove', function(){
  alert('При натисканні на кнопці ви увійдете в систему');
}, {once:true});

 const PHRASE = 'Ласкаво просимо!';

 function getRandomColor() {
     const r = Math.floor(Math.random() * 255);
     const g = Math.floor(Math.random() * 255);
     const b = Math.floor(Math.random() * 255);

     return `rgb(${r}, ${g}, ${b})`;
 }

 let root = document.getElementById('root')
 let h1 = document.createElement('h1')
 h1.textContent=PHRASE
 let btn = document.createElement('button')
 btn.textContent='Розфарбувати'
 root.prepend(h1)
 h1.after(btn)

 btn.addEventListener('click', () => {
  h1.innerHTML = '';
  for (let i = 0; i < PHRASE.length; i++) {
    h1.innerHTML += `<span style='color: ${getRandomColor()}'>${PHRASE[i]}</span>`;
  }
});
document.addEventListener('mousemove', () => {
  h1.innerHTML = ''
  for (let i = 0; i < PHRASE.length; i++) {
    h1.innerHTML += `<span style = 'color: ${getRandomColor()}'>
    ${
      PHRASE[i]
    }</span>`
  }
})

const block1 = document.querySelector('#block-1');
const block2 = document.querySelector('#block-2');

block1.addEventListener('mouseover', addColor(block2, 'green'));

block1.addEventListener('mouseout', removeColor(block2));

block2.addEventListener('mouseover', addColor(block1, 'red'));

block2.addEventListener('mouseout', removeColor(block1));


function addColor(block, color) {
    return () => block.style.backgroundColor = color;
}
function removeColor(block) {
    return () => block.style.backgroundColor = '';
}

let btnVal = document.querySelector("#validate-btn"); 
let input = document.querySelector("#input"); 
btnVal.addEventListener("click", function onButtonClick() { 
  if ( 
    input.value !== "" && 
    input.value.length >= 5 && 
    !input.value.includes(" ") && 
    checkSymbol() 
  ) { 
    // console.log(input.value.charCodeAt(0)); 
    input.style.outline = "1px solid green"; 
  } else { 
    input.style.outline = "1px solid red"; 
  } 
}); 
 
function checkSymbol() { 
  return ( 
    (input.value.charCodeAt(0) >= 65 && input.value.charCodeAt(0) <= 90) || 
    (input.value.charCodeAt(0) >= 97 && input.value.charCodeAt(0) <= 122) 
  ); 
}

let btn = document.querySelector('.counter')
btn.addEventListener('click',function(){

  btn.textContent=parseInt(this.textContent)+1
} )

let p = document.getElementById('counter');
let btnMinus = document.getElementById('decrement-btn');
let btnPlus = document.getElementById('increment-btn');

btnMinus.addEventListener('click', function () {
  let currentValue = parseInt(p.textContent);
  if (currentValue > 0) {
    p.textContent = currentValue - 1;
  } else {
    p.textContent = 0;
  }
});

btnPlus.addEventListener('click', function () {
  let currentValue = parseInt(p.textContent);
  p.textContent = currentValue + 1;
});

<input type="text" id="new-good-input">
      <button id="add-btn">Add</button>
      </div>
      <ul class="shopping-list">
     
      </ul>

let input = document.getElementById('new-good-input')
let btn = document.getElementById('add-btn')
let list = document.getElementsByClassName('shopping-list')[0]

btn.addEventListener('click', function(){
  if (input.value === '' || !isNaN(input.value)) {
    // Перевіряємо, чи поле вводу порожнє або чи введено число
    alert('Введіть коректне значення!')}
  else{
 let li = document.createElement('li')
 li.textContent=input.value
 list.prepend(li)
 input.value= ''}
})

const enterCounter = document.querySelector('#enter-counter');
const spaceCounter = document.querySelector('#space-counter');
const backSpaceCounter = document.querySelector('#backspace-counter');

let counter1 = 0;
let counter2 = 0;
let counter3 = 0;

window.addEventListener('keydown', (e) => {
    console.log(e.key);
        if (e.key === 'Enter') {
            counter1++
            enterCounter.textContent = counter1;
        } else if (e.code === 'Space') {
            counter2++
            spaceCounter.textContent = counter2;
        } else if (e.code === 'Backspace') {
            counter3++
            backSpaceCounter.textContent = counter3;
        }
    }
);
let btn = document.querySelector(".counter"); 
let counter = 0; 
 
const input = document.querySelector('#new-task')
const btn = document.querySelector('#clear')
const list = document.querySelector('.tasks-list')
input.addEventListener('keydown', (e) => {
  if (e.key === 'Enter' && input.value !== '') {
    let listItem = document.createElement('li')
    listItem.textContent = input.value
    list.append(listItem)
    input.value = ''
  }
})

window.addEventListener('keydown', (e) => {
  if (e.code === 'KeyD' && e.ctrlKey === true) {
    e.preventDefault()  
    list.lastElementChild.remove()
  }

  if (e.altKey === true && e.shiftKey === true && e.key === 'Backspace') {
    list.innerHTML = ''
  }
})

btn.addEventListener('click', () => {
  list.innerHTML = ''
})

const list = document.querySelector("#menu");
list.addEventListener("click", function onListClick(e) {
  e.preventDefault();

  if (e.target.tagName === "A") {
    const link = [...list.querySelectorAll("a")];
    link.forEach((el) => {
      el.classList.remove("active");
    });
    e.target.classList.add("active");
    console.log(link);
  }
});

let a = [...document.querySelectorAll("#elem")]
a.forEach(a=>{
  a.addEventListener ('click', (event)=>{
    event.preventDefault();
    a.textContent=a.href
  })
})
let input1 = document.getElementById('input1')
let input2 = document.getElementById('input2')
let p = document.getElementById("p")
let elem = document.getElementById('elem')

elem.addEventListener('click', ()=>{
  p.textContent=parseInt(input1.value) + parseInt(input2.value)
})

let elem = document.querySelector('#elem');

function func(surname, name) {
    console.log(this.value + ', ' + name + ' ' + surname);
}

// Використовуємо bind для створення нової функції з прив'язкою this до elem
let boundFunc = func.bind(elem);

boundFunc('John', 'Smit'); // тут повинно вивести 'hello, John Smit'
boundFunc('Eric', 'Luis'); // тут повинно вивести 'hello, Eric Luis'
function Question(questionText, questionAnswer) {
  this.questionText = questionText
  this.questionAnswer = questionAnswer

  this.getHtml = () => {
    const link = document.createElement('a')
    link.classList.add('question')
    link.style.display = 'block'
    link.textContent = this.questionText
    link.addEventListener('click', () => {
      if (text.style.display === 'block') {
        text.style.display = 'none'
      } else text.style.display = 'block'
    })

    const text = document.createElement('p')
    text.classList.add('answer')
    text.style.display = 'none'
    text.textContent = this.questionAnswer
    return [link, text]
  }
}

const question1 = new Question('Девиз дома Баратеонов', 'Нам ярость!')

const question2 = new Question('Сало', 'Мясо')

const root = document.querySelector('#root')

root.append(...question1.getHtml())

root.append(...question2.getHtml())


function Stopwatch(container) {
  this._time = 0;
  this.container = container;

  this.start = function () {
    this.intervalId = setInterval(() => {
      this._time++;
      this.setTime(this._time);
    }, 1000);
  };

  this.stop = function () {
    clearInterval(this.intervalId);
  };

  this.reset = function () {
    this._time = 0;
    this.setTime(this._time);
  };

  this.setTime = function (newTime) {
    const result = this.validateTime(newTime);
    if (result.status === "success") {
      this._time = newTime;
      this.container.textContent = this._time;
    } else {
      console.error(result.message);
    }
  };

  this.getTime = function () {
    return this._time;
  };

  this.validateTime = function (time) {
    if (Number.isInteger(time) && time >= 0) {
      return { status: "success" };
    } else {
      return {
        status: "error",
        message: "argument must be a positive integer",
      };
    }
  };
}

const startBtn = document.getElementById('start-time');
const stopBtn = document.getElementById('stop-time');
const resetBtn = document.getElementById('reset-time');

const stopWatchContainer = document.getElementById('time');
const stopwatch = new Stopwatch(stopWatchContainer);

startBtn.addEventListener('click', stopwatch.start.bind(stopwatch));
stopBtn.addEventListener('click', stopwatch.stop.bind(stopwatch));
resetBtn.addEventListener('click', stopwatch.reset.bind(stopwatch));*/

const a = 'farkd';
for (let i = a.length - 1; i >= 0; i--) {
  console.log(a[i]);
}