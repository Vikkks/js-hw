"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "img/imgs/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "img/imgs/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "img/imgs/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "img/imgs/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/imgs/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/imgs/trainer-f3.webp",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/imgs/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/imgs/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/imgs/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/imgs/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/imgs/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/imgs/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/imgs/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/imgs/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/imgs/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/imgs/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "img/imgs/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "img/imgs/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "img/imgs/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "img/imgs/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "img/imgs/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "img/imgs/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "img/imgs/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "img/imgs/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];

document.addEventListener('DOMContentLoaded', function () {
    const container = document.querySelector(".trainers-cards__container");
    const trainerCardTemplate = document.getElementById('trainer-card');
    const btnsSorting = document.querySelectorAll('.sorting__btn');
    const sidebar = document.querySelector(".sidebar");
    const sorting = document.querySelector('.sorting');

    sidebar.removeAttribute("hidden");
    sorting.removeAttribute('hidden');

    function renderTrainers(data) {
        container.innerHTML = '';
        data.forEach(trainer => appendTrainerToContainer(container, trainerCardTemplate, trainer));
    }

    function showAllTrainers() {
        renderTrainers(DATA);
    }

    function sortAndRender(sortFunction) {
        const sortedData = sortFunction(DATA);
        renderTrainers(sortedData);
    }

    function filterAndRender(category, specialization) {
        const filteredData = DATA.filter(trainer => (trainer.category === category || category === 'ВСІ') &&
                                                    (trainer.specialization === specialization || specialization === 'ВСІ'));
        renderTrainers(filteredData);
    }

    btnsSorting.forEach(btn => {
        btn.addEventListener('click', function () {
            btnsSorting.forEach(b => b.classList.remove('sorting__btn--active'));
            this.classList.add('sorting__btn--active');
            const sortingType = this.textContent.trim().toUpperCase();

            if (sortingType === 'ЗА ПРІЗВИЩЕМ') {
                sortAndRender(sortDataByLastName);
            } else if (sortingType === 'ЗА ДОСВІДОМ') {
                sortAndRender(sortDataByExperience);
            } else if (sortingType === 'ЗА ЗАМОВЧАННЯМ') {
                showAllTrainers();
            }
        });
    });

    container.addEventListener('click', function (event) {
        const target = event.target;
        if (target.classList.contains('trainer__show-more')) {
            const index = Array.from(target.parentNode.parentNode.children).indexOf(target.parentNode);
            const trainerData = DATA[index];
            showModal(trainerData);
        }
    });

    showAllTrainers();
});

function sortDataByLastName(data) {
    return data.slice().sort((a, b) => a["last name"].localeCompare(b["last name"], 'uk-UA'));
}

function sortDataByExperience(data) {
    return data.slice().sort((a, b) => parseInt(b.experience) - parseInt(a.experience));
}

function appendTrainerToContainer(container, template, trainer) {
    const clone = document.importNode(template.content, true);
    const trainerImg = clone.querySelector('.trainer__img');
    trainerImg.src = trainer.photo;
    trainerImg.alt = '';
    trainerImg.width = 280;
    trainerImg.height = 300;
    clone.querySelector('.trainer__name').textContent = `${trainer["first name"]} ${trainer["last name"]}`;
    container.appendChild(clone);
}

function showModal(trainerData) {
    document.documentElement.style.overflow = 'hidden';
    const modalInstance = createModal(trainerData);
    document.body.appendChild(modalInstance);
    const modalClose = document.querySelector('.modal__close');
    modalClose.addEventListener('click', function () {
        const modal = document.querySelector(".modal");
        modal.remove();
        document.documentElement.style.overflow = 'auto';
    });
}

function createModal(trainerData) {
    const modalTemplate = document.getElementById('modal-template');
    const modalContent = modalTemplate.content.cloneNode(true);

    if (trainerData) {
        modalContent.querySelector('.modal__img').src = trainerData.photo;
        modalContent.querySelector('.modal__name').textContent = `${trainerData["first name"]} ${trainerData["last name"]}`;
        modalContent.querySelector('.modal__point--category').textContent = `Категорія: ${trainerData.category}`;
        modalContent.querySelector('.modal__point--experience').textContent = `Досвід: ${trainerData.experience}`;
        modalContent.querySelector('.modal__point--specialization').textContent = `Напрям тренера: ${trainerData.specialization}`;
        modalContent.querySelector('.modal__text').textContent = trainerData.description;
    }

    return modalContent;
}

const filterSubmitBtn = document.getElementsByClassName('filters__submit')[0];

filterSubmitBtn.addEventListener('click', function() {
    
});


document.addEventListener('DOMContentLoaded', function () {
    const filtersForm = document.querySelector('.filters');

    filtersForm.addEventListener('submit', function (event) {
        event.preventDefault();

        // Отримати значення обраних напряму та категорії
        const selectedDirection = document.querySelector('input[name="direction"]:checked').value;
        const selectedCategory = document.querySelector('input[name="category"]:checked').value;

		
        if(selectedDirection==='kids club'){
			
		}
	 	else if (selectedDirection==='fight club'){

		}
		 else if (selectedDirection==='swimming pool'){
			const poolInstructors = clone.filter(trainer => trainer.specialization === 'Басейн');

console.log(poolInstructors);
		}
		else if (selectedDirection==='all'){
			
		}
		else if (selectedDirection==='gym'){
			
		}

        
    });
});