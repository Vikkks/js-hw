/* 
1. Метод event.preventDefault() у JavaScript використовується для відміни стандартної дії, яка пов'язана з подією. 
2. Замість того, щоб навішувати обробники подій на кожен окремий дочірній елемент, використання делегування дозволяє вам використовувати один обробник для групи елементів або для всього документу.
3. DOMContentLoaded, load, unload,  resize,  scroll, focus, blur.
*/

document.addEventListener('DOMContentLoaded', function () {
    let tabsTitles = document.querySelectorAll('.tabs-title');
    let tabsContents = document.querySelectorAll('.tabs-content li');

    tabsTitles.forEach(function (tabsTitle) {
        tabsTitle.addEventListener('click', () => {
            tabsContents.forEach(function (tabContent) {
                tabContent.style.display = 'none';
            })
            tabsTitles.forEach(function (tabTitle) {
                tabTitle.classList.remove('active')
            })
            let tabContentId = tabsTitle.dataset.content;
            let selectedTabContent = document.getElementById(tabContentId);
            if (selectedTabContent) {
                selectedTabContent.style.display = 'block'
                tabsTitle.classList.add('active')
            }
        })
    })
})