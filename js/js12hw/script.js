/* 
1. використати властивість code або key об'єкта події клавіатури в обробнику події keydown чи keyup.
2. event.code повертає фізичний код клавіші, а event.key: повертає символьне значення клавіші.
3. keydown: Ця подія виникає, коли користувач натиска клавішу на клавіатурі.keyup: Ця подія виникає, коли користувач відпускає клавішу, яку він або вона натискала. 
keypress: Ця подія теж виникає, коли користувач натискає клавішу, але вона виникає після події keydown.
Відмінність між keydown та keypress полягає в тому, що keypress не виникає для функціональних клавіш та клавіш, які не виділяють символів. Що до keyup, вона виникає після відпускання клавіші, але не надає інформації про символ.



- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. 
При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. 
Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. 
Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
<div class="btn-wrapper">
    <button class="btn">Enter</button>
		<button class="btn">S</button>
		<button class="btn">E</button>
		<button class="btn">O</button>
		<button class="btn">N</button>
		<button class="btn">L</button>
		<button class="btn">Z</button>
    <button class="btn">Tab</button>
	</div>
*/
let btnKeys = document.querySelectorAll('.btn');
document.addEventListener('keydown', function (e) {
    btnKeys.forEach(function (allBtn) {
        allBtn.style.backgroundColor = 'black';
    });
    let pressedKey = e.key.toUpperCase();
    let targetBtn = Array.from(btnKeys).find(btn => btn.textContent.toUpperCase() === pressedKey);
    if (targetBtn) {
        targetBtn.style.backgroundColor = 'blue';
    }
});