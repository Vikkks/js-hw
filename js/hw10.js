//1.document.createElement(),insertAdjacentHTML(),innerHTML,document.createTextNode().
//2. отримати посилання на елемент, превірка чи він існує, видалення.
/*const navigationEl = document.querySelector('.navigation');
if (navigationEl) {
 navigationEl.remove();
} {
  console.error('Елемент з класом "navigation" не знайдено.');
}
//3. insertBefore(),insertAdjacentHTML(),appendChild() та prepend(),after() та before().

Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.*/

const a = document.createElement('a')
a.href='#'
a.textContent='Learn More'
const footer = document.getElementsByTagName('footer')[0]
footer.appendChild(a)

const select = document.createElement('select')
select.id='rating'
const features = document.querySelector('.features')
features.parentNode.insertBefore(select, features);

for(let i = 0; i<4; i++){
    let option = document.createElement('option')
    option.value=4-i+' stars'
    option.textContent=4-i+' stars'
    select.prepend(option)  
}