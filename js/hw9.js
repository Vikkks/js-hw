//1.DOM - це структуроване представлення веб-сторінки або XML-документа в браузері, яке дозволяє js, змінювати структуру, стиль і вміст документа.
//2.основною відмінністю є те, що іnnerHTML працює з HTML-структурою, в той час як innerText просто оперує текстом, ігноруючи HTML-теги.
//3. document.getElementById('ідентифікатор_елемента'), ..querySelector('тег_елемента'),  .querySelectorAll('тег_елемента'), getElementsByTagName('тег_елемента'),.getElementsByClassName('клас_елемента')/
//4. різниця між NodeList та HTMLCollection може бути незначною, і можна  використовувати їх подібно. Однак, якщо нам потрібні якісь особливості, такі як жива або нежива колекція, чи тип вузлів, то вам слід ретельно вибирати між ними.

//1

let features = document.querySelectorAll('.feature');
let featuresByClass = document.getElementsByClassName('feature');
features.forEach(function(feature) {
    feature.style.textAlign = 'center';
});

//2

let title2 = document.getElementsByTagName('h2')
for (let i = 0; i < title2.length; i++) {
    title2[i].textContent = 'Awesome feature';
    title2[i].style.color = 'red';}

//3

let featureTitle = document.querySelectorAll('.feature-title')
featureTitle.forEach(function(featureTitle) {
    featureTitle.innerHTML += '!'
})
