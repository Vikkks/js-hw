//1.рядки можна створити за допомогою лапок (одинарних або подвійних) або за допомогою шаблонних рядків.
//2.одинарні і подвійні є схоживи типами лапок, а шаблонні рядки є новішим типом лапок, які дозволяють вбудовувати значення змінних або вирази в рядок за допомогою вираження ${}.
//При цьому можна використовувати і одинарні, і подвійні лапки в середині шаблонного рядка.
//3.можна порівняти за допомогою операторів рівності (=== або !==), або методом localeCompare().
//4.Метод Date.now() у JS повертає кількість мілісекунд, що пройшли з 1 січня 1970 року 00:00:00  до поточного моменту часу. Це число є числовим представленням моменту часу (timestamp)
//5.Date.now() повертає числове значення timestamp, тоді як new Date() створює об'єкт Date, який може бути використаний для отримання різних частин часу 

/*function isPalindrome(str) {
    // Видаляємо пробіли та перетворюємо рядок у нижній регістр
    const cleanedStr = str.replace(/\s/g, '').toLowerCase();

    // Перевертаємо рядок
    const reversedStr = cleanedStr.split('').reverse().join('');

    // Порівнюємо оригінальний рядок та перевернутий
    return cleanedStr === reversedStr;
}

// Приклад використання:
const inputString = "A man a plan a canal Panama";
const result = isPalindrome(inputString);

console.log(result); // Виведе true, оскільки "A man a plan a canal Panama" є паліндромом.

function checkStringLength(str, maxLength) {
    return str.length <= maxLength;
  }
  
  // Приклади використання:
  console.log(checkStringLength('checked string', 20)); // true
  console.log(checkStringLength('checked string', 10)); // false"*/

  
  function calculateAge() {
    const birthDate = prompt('Введіть свою дату народження у форматі YYYY-MM-DD:');
    
    if (!birthDate) {
      return 'Введення скасовано або неправильний формат дати.';
    }
  
    const currentDate = new Date();
    const birthDateObject = new Date(birthDate);
    
    const age = currentDate.getFullYear() - birthDateObject.getFullYear();
  
    // Перевірка, чи день та місяць народження вже пройшли у поточному році
    if (
      currentDate.getMonth() < birthDateObject.getMonth() ||
      (currentDate.getMonth() === birthDateObject.getMonth() &&
        currentDate.getDate() < birthDateObject.getDate())
    ) {
      return age - 1;
    }
  
    return age;
  }  const userAge = calculateAge();
  console.log(userAge);