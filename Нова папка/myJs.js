
/*
  let BASE_URL = "https://ajax.test-danit.com/api/swapi/planets"; 
let root = document.querySelector("#root"); 
 
class Planets { 
  getPlanets(url) { 
    return new Promise((resolve, reject) => { 
      const xhr = new XMLHttpRequest(); 
 
      xhr.open("GET", `${url}`); 
      xhr.send(); 
 
      xhr.onload = () => { 
        if (xhr.readyState === 4 && xhr.status === 200) { 
          resolve(JSON.parse(xhr.response)); 
        } else { 
          reject(new Error("data error")); 
        } 
      }; 
 
      xhr.onerror = (e) => { 
        reject(e); 
      }; 
    }); 
  } 
  createList(planets) { 
    let ul = document.createElement("ul"); 
    const sortedPlanets = planets.toSorted( 
      (planetOne, planetTwo) => planetTwo.diameter - planetOne.diameter 
    ); 
    let newPlanets = sortedPlanets.map(({ name, diameter }) => { 
      let li = document.createElement("li"); 
      li.textContent = name + " " + diameter; 
      return li; 
    }); 
    ul.append(...newPlanets); 
    return ul; 
  } 
} 
 
let planet = new Planets(); 
 
planet 
  .getPlanets(BASE_URL) 
  .then((planets) => { 
    root.append(planet.createList(planets)); 
  }) 
  .catch((e) => { 
    console.log(e); 
  });
*/

