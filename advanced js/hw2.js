/*1

try {
    const result = operationThatMightFail();
    // Обробка результату
} catch (error) {
    console.error('Помилка при виконанні операції:', error);
}

try {
     result = operationThatMightFail();
    // Обробка результату
} catch (error) {
    console.error('Помилка при виконанні операції:', error);
}

try {
    const userInput = parseInt(prompt('Введіть число:'));
    if (isNaN(userInput)) {
        throw new Error('Введено не число');
    }
    // Обробка введеного числа
} catch (error) {
    console.error('Помилка під час обробки користувацького вводу:', error);
}*/


const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];


    function validateBooks(books) {
      const requiredProps = ['author', 'name', 'price'];

      books.forEach(book => {
        try {
          requiredProps.forEach(prop => {
            if (!(prop in book)) {
              throw new Error(`Об'єкт не містить властивість "${prop}"`);
            }
          });
          renderBook(book);
        } catch (error) {
          console.error(error.message);
        }
      });
    }


    function renderBook(book) {
      const root = document.getElementById('root');
      const ul = document.createElement('ul');
      const li = document.createElement('li');
      li.textContent = `Author: ${book.author}, Name: ${book.name}, Price: ${book.price}`;
      ul.appendChild(li);
      root.appendChild(ul);
    }
    validateBooks(books);

    const newBook = {
      author: "Станіслав Лем",
      name: "Соляріс",
      price: 40
    };
    
    books.push(newBook);
    renderBook(newBook);


