/*Деструктуризація - це процес розбиття складної структури, такої як об'єкт або масив, на окремі складові частини. 
Це може бути корисним в багатьох випадках, особливо при роботі з даними чи об'єктами в програмуванні.
Зручність коду: Вона дозволяє звертатися до окремих елементів складної структури безпосередньо, що робить код більш зрозумілим та легким для розуміння.
Ефективність: Деструктуризація дозволяє одночасно отримати доступ до кількох значень чи елементів структури, зменшуючи кількість необхідного коду.
Переорганізація даних: Це допомагає перетворити дані з одного формату на інший, обмінюючись даними між різними частинами програми.
Робота з функціями: Деструктуризація може бути корисною при передачі параметрів у функції або поверненні значень з неї, оскільки дозволяє працювати зі структурованими даними зручним способом.
*/

//1.
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const mergedClientsSet = new Set([...clients1, ...clients2]);

const mergedClientsArray = [...mergedClientsSet];

console.log(mergedClientsArray);

//2
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

  const charactersShortInfo = characters.map(character => ({
    name: character.name,
    lastName: character.lastName,
    age: character.age
  }));
  
  console.log(charactersShortInfo);

  //3
  const user1 = {
    name: "John",
    years: 30
  };
  
  const { name: ім_я, years: вік, isAdmin = false } = user1;
console.log(ім_я);
console.log(вік);
console.log(isAdmin);

//4

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  };
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  };
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  };
  
  const fullProfile = Object.assign({}, satoshi2018, satoshi2019, satoshi2020);
  
  console.log(fullProfile);

  //5

  const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  };
  
  const newBooks = books.concat(bookToAdd);
  
  console.log(newBooks);

  //6

  const employee = {
    name: 'John Doe',
    position: 'Software Developer'
  };
    const newEmployee = {
    ...employee,
    age: 30,
    salary: 50000
  };
  
  console.log(newEmployee);

  //7

  const array = ['value', () => 'showValue'];

const value = array[0]; 
const showValue = array[1]; 

console.log(value); 
console.log(showValue()); 