//1. Кожен об'єкт має прототип, який визначає набір властивостей та методів, які об'єкт може використовувати. 
//Коли властивість або метод не знаходяться безпосередньо в об'єкті, JavaScript шукає їх у його прототипі, і так далі вгору по ланцюжку прототипів.
//2.Виклик super() у конструкторі класу-нащадка в JavaScript використовується для виклику конструктора класу-батька (або суперкласу).
/*
class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
   // Гетер для властивості name
   get name() {
    return this._name;
  }

  // Сетер для властивості name
  set name(newName) {
    this._name = newName;
  }

  // Гетер для властивості age
  get age() {
    return this._age;
  }

  // Сетер для властивості age
  set age(newAge) {
    this._age = newAge;
  }

  // Гетер для властивості salary
  get salary() {
    return this._salary;
  }

  // Сетер для властивості salary
  set salary(newSalary) {
    this._salary = newSalary;
  }
   
  }
  
  let employee1 = new Employee('stef', 21, 20);
  let employee2 = new Employee('ivan', 16, 0);
  
  console.log(employee1);
  console.log(employee2);
  
  employee1.name = "Jane Smith";
  employee1.age = 25;
  employee1.salary = 60000;
  
  console.log(employee1);

  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this.lang = lang;
    }
  
    // Перезапис гетера для властивості salary
    get salary() {
      return super.salary * 3;
    }
  }
  
  // Створення екземплярів класу Programmer
  const programmer1 = new Programmer('John', 30, 50000, ['JavaScript', 'Python']);
  const programmer2 = new Programmer('Alice', 25, 60000, ['Java', 'C++']);
  
  // Виведення інформації про програмістів у консоль
  console.log(programmer1.lang);
  console.log(programmer2);
*/


