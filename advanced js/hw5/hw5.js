
class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
  }

  render() {
    const card = document.createElement('div');
    card.classList.add('card');
    card.innerHTML = `
    <p>Author: ${this.user.name} (${this.user.email})</p>
      <h2>${this.post.title}</h2>
      <p>${this.post.body}</p>
      <button onclick="deletePost(${this.post.id})">Delete</button>
    `;
    return card;
  }
}

function deletePost(postId) {
  fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
    method: 'DELETE'
  })
  .then(response => {
    if (!response.ok) {
      throw new Error('Failed to delete post');
    }
    const postCard = document.getElementById(`post-${postId}`);
    postCard.remove();
  })
  .catch(error => {
    console.error(error);
  });
}

document.addEventListener('DOMContentLoaded', () => {
  const root = document.getElementById('root');
  
  fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => {
      if (!response.ok) {
        throw new Error('Failed to fetch users');
      }
      return response.json();
    })
    .then(users => {
      // Отримання списку публікацій
      fetch('https://ajax.test-danit.com/api/json/posts')
        .then(response => {
          if (!response.ok) {
            throw new Error('Failed to fetch posts');
          }
          return response.json();
        })
        .then(posts => {
          posts.forEach(post => {
            const user = users.find(user => user.id === post.userId);
            if (user) {
              const card = new Card(post, user).render()
              card.id = `post-${post.id}`
              root.appendChild(card)
            }
          });
        })
        .catch(error => {
          console.error(error);
        });
    })
    .catch(error => {
      console.error(error);
    });
});