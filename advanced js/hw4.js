/*1.

AJAX, або асинхронний JavaScript і XML, - це технологія, що дозволяє веб-сторінкам взаємодіяти з сервером без перезавантаження сторінки. 
Завдяки AJAX, ви можете виконувати HTTP-запити до сервера з JavaScript і отримувати або відправляти дані асинхронно.*/







let BASE_URL = "https://ajax.test-danit.com/api/swapi/films"; 
let root = document.querySelector("#root"); 

async function fetchData(url) {
  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return await response.json();
  } catch (error) {
    throw new Error('Error fetching data: ' + error.message);
  }
}

async function displayFilmsAndCharactrs() {
  try {
    const films = await fetchData(BASE_URL);
    const ul = document.createElement('ul')
    ul.classList.add('films-list')
    for (const film of films) {
      const li = document.createElement('li');
      li.innerHTML = `
        <strong>Номер епізоду:</strong> ${film.episodeId}<br>
        <strong>Назва:</strong> ${film.name}<br>
        <strong>Зміст:</strong> ${film.openingCrawl}<br>
      `;
      ul.appendChild(li)
      displayCharactrs(film.characters, li);
    }

    // додавання списку фільмів до кореневого елементу
    root.appendChild(ul);
  } catch (error) {
    console.error(error)
  }
}

async function displayCharactrs(characterUrls, filmListItem) {
  try {
    // Отримання списку персонажів
    const characters = await Promise.all(
      characterUrls.map((characterUrl) => fetchData(characterUrl))
    );
    const charactersUl = document.createElement('ul')

    characters.forEach((character) => {
      const characterLi = document.createElement('li');
      characterLi.textContent = character.name;
      charactersUl.appendChild(characterLi);
    });

    filmListItem.appendChild(charactersUl);
  } catch (error) {
    console.error(error);
  }
}

displayFilmsAndCharactrs();

