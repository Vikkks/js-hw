// асинхронність цеколи програмні операції відбуваються паралельно або в нечітокму порядку. коли одна подія почала оброблятись нечекаючи завершення попередньої.



  
  document.getElementById('findIpButton').addEventListener('click', async () => {
    try {
        // Отримання IP-адреси клієнта
        const clientIpResponse = await fetch('https://api.ipify.org/?format=json');
        const clientIpData = await clientIpResponse.json();
        const clientIp = clientIpData.ip;
        console.log(clientIp)
    
        // Отримання інформації про фізичну адресу за IP-адресою
        const locationResponse = await fetch(`https://ipapi.co/${clientIp}/json/`);
        const locationData = await locationResponse.json();
    
        const locationInfo = `
            <p><strong>Континент:</strong> ${locationData.continent_name}</p>
            <p><strong>Країна:</strong> ${locationData.country_name}</p>
            <p><strong>Регіон:</strong> ${locationData.region}</p>
            <p><strong>Місто:</strong> ${locationData.city}</p>
            <p><strong>Поштовий код:</strong> ${locationData.postal}</p>
        `;
        document.getElementById('locationInfo').innerHTML = locationInfo;
    } catch (error) {
        console.error(error);
    }
  });